# Requirements

- [X] user must see arbitrary RSS feed as a blog
- [X] user must listen to audio in the post
- [X] user must watch video in a post
- [X] user must see pictures in a post
- [X] user must click links in a post
- user must click on 'RSS' button to subscribe
- user must open a specific post from URL
- user can comment under a post
- user can authenticate
