# Design

RSS reader in the form of a blog

competes: rss readers, blog platforms

interacts: with git hosting providers

constitutes: web application

includes: 

patterns: 

resembles: 

stakeholders: fetsorn

RSS file is fetched from a link. html and markdown from an RSS feed file is rendered as a blog.
A link to RSS file can be specified in the URL.

## ideas
for comments, user can authenticate with a public key from metamask, and edit a common csvs database
for encrypted data, users can authenticate with a public key from metamask and decrypt a multiparty encryption schema like [age](https://github.com/FiloSottile/age) or [rage](https://github.com/str4d/rage).
